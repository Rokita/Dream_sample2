/*
 * Application.c
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#include "Application/Application.h"

StateMachine_T kDreamStateMachine;

extern Handler SM_Function;

void vApplicationInit(void)
   {
   DreamExperiment_StructInitialization();
   kDreamStateMachine.CurrentState = StateMachine_StateInit;
   /*
    * if(IS_PIN_READ_RECOVERY_DATA_LOW())
    * {
    *   kDreamStateMachine.CurrentState = StateMachine_StateReadRecoveryData;
    * }
    * else
    * {
    * //Read Last State from Memory and set StateMachine.currentStae
    *    kDreamStateMachine.CurrentState = StateMachine_StateInit;
    * }
    */
   }

void vApplicationMain(void)
   {
   SM_Function[kDreamStateMachine.CurrentState]();
   }
