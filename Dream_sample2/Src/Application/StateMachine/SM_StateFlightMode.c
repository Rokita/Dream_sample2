/*
 * SM_StateFlightMode.c
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#include "Application/StateMachine/SM_StateFlightMode.h"

void FlightSM_Init(void);               //Just for initialization
void FlightSM_Countdown(void);          //Countdown time before signals
void FlightSM_StartOfDataStorage(void);  //During countdown time after SODS signal - start of data storage
void FlightSM_LiftOff(void);            //This is time when rocket is starting after LO signal - LiftOff
void FlightSM_StartOfExperiment(void);  //This is time when Dream is in microgravity area - Start of experiment
void FlightSM_BackToZeroPosition(void);      //this is time when platform is going back to position zero
void FlightSM_EndOfExperiment(void);    //this is time when we finish drilling experiment and we can be TurnOff

/**********************************************
 *                Dream State                 *
 *********************************************/

void SM_StateFlightMode(void)
   {
   vBoard_LED_Toggle(1);

   FlightModeSM_T eFlightModeState;
   DreamExperiment_getFlightModeState(&eFlightModeState);

   switch(eFlightModeState)
      {
      case FlightModeSM_StateInit:
         FlightSM_Init();
         break;
      case FlightModeSM_StateCountdown:
         FlightSM_Countdown();
         break;
      case FlightModeSM_StateSODS:
         FlightSM_StartOfDataStorage();
         break;
      case FlightModeSM_StateLO:
         FlightSM_LiftOff();
         break;
      case FlightModeSM_StateSOE:
         FlightSM_StartOfExperiment();
         break;
      case FlightModeSM_StateBTZP:
         FlightSM_BackToZeroPosition();
         break;
      case FlightModeSM_StateEOF:
         FlightSM_EndOfExperiment();
         break;
      default:
         break;
      }
   }

/**********************************************
 *                    Substates               *
 *********************************************/

void FlightSM_Init(void)
   {
   vBoard_LED_Set(2, 1);

   DreamExperiment_setDrillingMotorState(SystemState_Disable);
   DreamExperiment_setDrillingMotorDirection(MotorDirection_Right);

   DreamExperiment_setLinearMotorState(SystemState_Disable);
   DreamExperiment_setLinearMotorMode(LinearMotorMode_32Microsteps);
   DreamExperiment_setLinearMotorSpeed(2000);
   DreamExperiment_setLinearMotorDirection(MotorDirection_Right);

   DreamExperiment_setFlightModeState(FlightModeSM_StateCountdown);
   }

void FlightSM_Countdown(void)
   {
   vBoard_LED_Set(3, 1);

   DreamExperiment_setDrillingMotorState(SystemState_Disable);
   DreamExperiment_setDrillingMotorDirection(MotorDirection_Right);

   DreamExperiment_setLinearMotorState(SystemState_Disable);
   DreamExperiment_setLinearMotorMode(LinearMotorMode_32Microsteps);
   DreamExperiment_setLinearMotorSpeed(2000);
   DreamExperiment_setLinearMotorDirection(MotorDirection_Right);

   Signal_T eSignal;
   DreamExperiment_getSODSsignal(&eSignal);
   if(Signal_On == eSignal)
      {
      DreamExperiment_setFlightModeState(FlightModeSM_StateSODS);
      }

   DreamExperiment_getLOsignal(&eSignal);
   if(Signal_On == eSignal)
      {
      DreamExperiment_setFlightModeState(FlightModeSM_StateLO);
      }
   }

void FlightSM_StartOfDataStorage(void)
   {
   vBoard_LED_Set(4, 1);

   DreamExperiment_setDrillingMotorState(SystemState_Disable);
   DreamExperiment_setDrillingMotorDirection(MotorDirection_Right);

   DreamExperiment_setLinearMotorState(SystemState_Disable);
   DreamExperiment_setLinearMotorMode(LinearMotorMode_32Microsteps);
   DreamExperiment_setLinearMotorSpeed(2000);
   DreamExperiment_setLinearMotorDirection(MotorDirection_Right);

   //Start saving data in Memory
   Signal_T eSignal;

   DreamExperiment_getLOsignal(&eSignal);
   if(Signal_On == eSignal)
      {
      DreamExperiment_setFlightModeState(FlightModeSM_StateLO);
      }
   }

void FlightSM_LiftOff(void)
   {
   vBoard_LED_Toggle(2);

   DreamExperiment_setDrillingMotorState(SystemState_Disable);
   DreamExperiment_setDrillingMotorDirection(MotorDirection_Right);

   DreamExperiment_setLinearMotorState(SystemState_Disable);
   DreamExperiment_setLinearMotorMode(LinearMotorMode_32Microsteps);
   DreamExperiment_setLinearMotorSpeed(2000);
   DreamExperiment_setLinearMotorDirection(MotorDirection_Right);

   //Wait for signal or wait time
   Signal_T eSignal;

   DreamExperiment_getSOEsignal(&eSignal);
   if(Signal_On == eSignal)
      {
      DreamExperiment_setFlightModeState(FlightModeSM_StateSOE);
      }
   }

void FlightSM_StartOfExperiment(void)
   {
   vBoard_LED_Toggle(3);

   DreamExperiment_setDrillingMotorState(SystemState_Enable);
   DreamExperiment_setDrillingMotorDirection(MotorDirection_Right);

   DreamExperiment_setLinearMotorState(SystemState_Enable);
   DreamExperiment_setLinearMotorMode(LinearMotorMode_16Microsteps);
   DreamExperiment_setLinearMotorSpeed(3000);
   DreamExperiment_setLinearMotorDirection(MotorDirection_Right);

   //DrillingMotor and LinearMotor start
   //If drillingMotor current is to high execute redrilling function

   LimitSwitch_T eLimitSwitch;
   DreamExperiment_getLimitSwitchMax(&eLimitSwitch);

   if(LimitSwitch_On == eLimitSwitch)  //jesli sie przewiercilismy to czas wracac do pozycji zero
      {
      DreamExperiment_setFlightModeState(FlightModeSM_StateBTZP);
      }
   }

void FlightSM_BackToZeroPosition(void)
   {
   vBoard_LED_Toggle(4);

   DreamExperiment_setDrillingMotorState(SystemState_Enable);
   DreamExperiment_setDrillingMotorDirection(MotorDirection_Right);

   DreamExperiment_setLinearMotorState(SystemState_Enable);
   DreamExperiment_setLinearMotorMode(LinearMotorMode_4Microsteps);
   DreamExperiment_setLinearMotorSpeed(2000);
   DreamExperiment_setLinearMotorDirection(MotorDirection_Left);

   LimitSwitch_T eLimitSwitch;
   DreamExperiment_getLimitSwitchZero(&eLimitSwitch);

   if(LimitSwitch_On == eLimitSwitch)  //jesli jestesmy juz na pozycji zero to wylaczamy silniki
      {
      DreamExperiment_setFlightModeState(FlightModeSM_StateSOE);
      }
   }

void FlightSM_EndOfExperiment(void)
   {

   DreamExperiment_setDrillingMotorState(SystemState_Disable);
   DreamExperiment_setDrillingMotorDirection(MotorDirection_Right);

   DreamExperiment_setLinearMotorState(SystemState_Disable);
   DreamExperiment_setLinearMotorMode(LinearMotorMode_32Microsteps);
   DreamExperiment_setLinearMotorSpeed(2000);
   DreamExperiment_setLinearMotorDirection(MotorDirection_Right);

   vBoard_LED_Set(2, 0);
   vBoard_LED_Set(3, 0);
   vBoard_LED_Set(4, 0);
   SM_transitionTo(StateMachine_StateReadRecoveryData);
   }
