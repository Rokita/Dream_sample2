/*
 * StateMachine.c
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#include "Application/StateMachine/StateMachine.h"

extern StateMachine_T kDreamStateMachine;

Handler SM_Function =
   {
   SM_StateInit, SM_StateTestMode, SM_StateAutomaticTestMode, SM_StateFlightMode, SM_StateReadRecoveryData
   };

void SM_transitionTo(State_T nextState)
   {

   /*save new state in Memory*/

   kDreamStateMachine.LastState = kDreamStateMachine.CurrentState;
   kDreamStateMachine.CurrentState = nextState;
   }

State_T SM_GetCurrentState(void)
   {
   return kDreamStateMachine.CurrentState;
   }

State_T SM_GetLastState(void)
   {
   return kDreamStateMachine.LastState;
   }

