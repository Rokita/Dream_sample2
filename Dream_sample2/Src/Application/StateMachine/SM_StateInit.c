/*
 * SM_StateInit.c
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#include "Application/StateMachine/SM_StateInit.h"

void SM_StateInit(void)
   {
   //Read from Memory last parameters
   /*
    * switch(lastParameters.SM_State)
    * {
    *   case  StateMachine_StateInit:
    *      SM_transitionTo(StateMachine_StateInit);
    *      break;
    *   case  StateMachine_StateTestMode:
    *      SM_transitionTo(StateMachine_StateTestMode);
    *      break;
    *   case  StateMachine_StateAutomaticTestMode:
    *      SM_transitionTo(StateMachine_StateAutomaticTestMode);
    *      break;
    *   case  StateMachine_StateFlightMode:
    *      SM_transitionTo(StateMachine_StateFlightMode);
    *      break;
    *   default:
    *   break;
    * }
    */
   SM_transitionTo(StateMachine_StateFlightMode);
   }
