/*
 * SM_StateReadRecoveryData.c
 *
 *  Created on: 12 pa� 2016
 *      Author: Kamil
 */


#include "Application/StateMachine/SM_StateInit.h"

void SM_StateReadRecoveryData(void)
   {
   vBoard_LED_Toggle(1);
   vBoard_LED_Toggle(2);
   vBoard_LED_Toggle(3);
   vBoard_LED_Toggle(4);
   SM_transitionTo(StateMachine_StateInit);
   }
