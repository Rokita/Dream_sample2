/*
 * SM_StateTestMode.c
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#include "Application/StateMachine/SM_StateTestMode.h"

void SM_StateTestMode(void)
   {
   vBoard_LED_Toggle(2);
   SM_transitionTo(StateMachine_StateAutomaticTestMode);
   }
