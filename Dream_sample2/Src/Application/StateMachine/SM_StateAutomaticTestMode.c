/*
 * SM_StateAutomaticTestMode.c
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#include "Application/StateMachine/SM_StateAutomaticTestMode.h"

void SM_StateAutomaticTestMode(void)
   {
   vBoard_LED_Toggle(3);
   SM_transitionTo(StateMachine_StateFlightMode);

   }
