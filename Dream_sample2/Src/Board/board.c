/*
 * board.c
 *
 *  Created on: 3 pa� 2016
 *      Author: Kamil
 */

#include "Board/board.h"

#define AD8418_Gain 20        //Current sensor gain 20 V/V
#define AD8418_Res 0.1        //Resistor 0.1 Ohm
#define MCP9700_TempCoefficient 0.01  //10mV/*C

/************************************
 *       PRIVATE FUNCTIONS          *
 ***********************************/
void vBoard_DrillingMotor_SetState(const SystemState_T eSystemState);
void vBoard_DrillingMotor_SetDirection(const MotorDirection_T);
void vBoard_DrillingMotor_CurrentMeasurement(uint16_t *);
void vBoard_DrillingMotor_SpeedMeasurement(uint8_t *);
void vBoard_DrillingMotor_TemperatureMeasurement(int16_t *);

void vBoard_LinearMotor_SetState(const SystemState_T eSystemState);
void vBoard_LinearMotor_SetDirection(const MotorDirection_T);
void vBoard_LinearMotor_SetMode(const LinearMotorMode_T eLinearMode);
void vBoard_LinearMotor_CheckFault(void);
void vBoard_LinearMotor_TemperatureMeasurement(int16_t *);
void vBoard_LinearMotor_SetSpeed(const uint16_t u16LinearSpeed);

/*****************************************************
 *                BOARD FUNCTIONS                    *
 ****************************************************/

///////////////////////////////////////////////////////////////////////////////////
void vBoard_LED_Set(const uint8_t LedNumber, const uint8_t state)
///////////////////////////////////////////////////////////////////////////////////
   {
   if(LedNumber == 1)
      {
      HAL_GPIO_WritePin(Led1_GPIO_Port, Led1_Pin, state ? SET : RESET);
      }
   if(LedNumber == 2)
      {
      HAL_GPIO_WritePin(Led2_GPIO_Port, Led2_Pin, state ? SET : RESET);
      }
   if(LedNumber == 3)
      {
      HAL_GPIO_WritePin(Led3_GPIO_Port, Led3_Pin, state ? SET : RESET);
      }
   }

///////////////////////////////////////////////////////////////////////////////////
void vBoard_LED_Toggle(const uint8_t LedNumber)
///////////////////////////////////////////////////////////////////////////////////
   {
   if(LedNumber == 1)
      {
      HAL_GPIO_TogglePin(Led1_GPIO_Port, Led1_Pin);
      }
   if(LedNumber == 2)
      {
      HAL_GPIO_TogglePin(Led2_GPIO_Port, Led2_Pin);
      }
   if(LedNumber == 3)
      {
      HAL_GPIO_TogglePin(Led3_GPIO_Port, Led3_Pin);
      }
   }
/*****************************************************
 *                SIGNALS FUNCTIONS                  *
 ****************************************************/

///////////////////////////////////////////////////////////////////////////////////
void vBoard_Signals_Update(void)
///////////////////////////////////////////////////////////////////////////////////
   {
   //LimitSwitch values start with 0 (zero) value.
   static uint16_t S_LO = 0;
   static uint16_t S_SOE = 0;
   static uint16_t S_SODS = 0;

   Signal_T eSignal;

   // if pin is in High state then value is switch 1 position left (LSB bit is set to 0)
   // if pin is in Low state then value is switch 1 position left and LSB bit is set to 1
   S_LO = HAL_GPIO_ReadPin(SignalLo_GPIO_Port, SignalLo_Pin) ? S_LO << 1 : (S_LO << 1) | 1;
   S_SOE = HAL_GPIO_ReadPin(SignalSoe_GPIO_Port, SignalSoe_Pin) ? S_SOE << 1 : (S_SOE << 1) | 1;
   S_SODS = HAL_GPIO_ReadPin(SignalSods_GPIO_Port, SignalSods_Pin) ? S_SODS << 1 : (S_SODS << 1) | 1;

   if(S_LO == 0xFFFF || S_LO == 0x0000)
      {
      eSignal = S_LO ? Signal_On : Signal_Off;
      DreamExperiment_setLOsignal(eSignal);
      }
   if(S_SODS == 0xFFFF || S_SODS == 0x0000)
      {
      eSignal = S_SODS ? Signal_On : Signal_Off;
      DreamExperiment_setSODSsignal(eSignal);
      }
   if(S_SOE == 0xFFFF || S_SOE == 0x0000)
      {
      eSignal = S_SOE ? Signal_On : Signal_Off;
      DreamExperiment_setSOEsignal(eSignal);
      }
   }

/*****************************************************
 *            LIMIT SWITCH FUNCTIONS                 *
 ****************************************************/

///////////////////////////////////////////////////////////////////////////////////
void vBoard_LimitSwitch_Update(void)
///////////////////////////////////////////////////////////////////////////////////
   {
   //LimitSwitch values start with 0 (zero) value.
   static uint8_t u8LimitSwitch_Zero = 0;
   static uint8_t u8LimitSwitch_Max = 0;

   LimitSwitch_T eLimitSwitch;

   // if pin is in High state then value is switch 1 position left (LSB bit is set to 0)
   // if pin is in Low state then value is switch 1 position left and LSB bit is set to 1
   u8LimitSwitch_Zero = HAL_GPIO_ReadPin(LimitSwitchZero_GPIO_Port, LimitSwitchZero_Pin) ? u8LimitSwitch_Zero << 1 : (u8LimitSwitch_Zero << 1) | 1;
   u8LimitSwitch_Max = HAL_GPIO_ReadPin(LimitSwitchMax_GPIO_Port, LimitSwitchMax_Pin) ? u8LimitSwitch_Max << 1 : (u8LimitSwitch_Max << 1) | 1;

   if(u8LimitSwitch_Zero == 0xFF || u8LimitSwitch_Zero == 0x00)
      {
      eLimitSwitch = u8LimitSwitch_Zero ? LimitSwitch_On : LimitSwitch_Off;
      DreamExperiment_setLimitSwitchZero(eLimitSwitch);
      }
   if(u8LimitSwitch_Max == 0xFF || u8LimitSwitch_Max == 0x00)
      {
      eLimitSwitch = u8LimitSwitch_Max ? LimitSwitch_On : LimitSwitch_Off;
      DreamExperiment_setLimitSwitchMax(eLimitSwitch);
      }
   }

/*****************************************************
 *          DRILLING MOTOR FUNCTIONS                 *
 ****************************************************/

///////////////////////////////////////////////////////////////////////////////////
void vBoard_DrillingMotor_Update()
///////////////////////////////////////////////////////////////////////////////////
   {
   uint16_t u16Current_mA;
   vBoard_DrillingMotor_CurrentMeasurement(&u16Current_mA);
   DreamExperiment_setDrillingMotorCurrent(u16Current_mA);

   uint8_t u8Speed = 0;
   vBoard_DrillingMotor_SpeedMeasurement(&u8Speed);
   DreamExperiment_setDrillingMotorMeasuredSpeed(u8Speed);

   int16_t i16Temp;
   vBoard_DrillingMotor_TemperatureMeasurement(&i16Temp);
   DreamExperiment_setDrillingMotorTemperature(i16Temp);

   SystemState_T eSystemState;
   DreamExperiment_getDrillingMotorState(&eSystemState);
   vBoard_DrillingMotor_SetState(eSystemState);

   MotorDirection_T eMotorDirection;
   DreamExperiment_getDrillingMotorDirection(&eMotorDirection);
   vBoard_DrillingMotor_SetDirection(eMotorDirection);
   }

///////////////////////////////////////////////////////////////////////////////////
void vBoard_DrillingMotor_SetDirection(const MotorDirection_T eMotorDirection)
///////////////////////////////////////////////////////////////////////////////////
   {
   if(MotorDirection_Left == eMotorDirection)
      {
      HAL_GPIO_WritePin(DrillingDir_GPIO_Port, DrillingDir_Pin, SET);
      }
   if(MotorDirection_Right == eMotorDirection)
      {
      HAL_GPIO_WritePin(DrillingDir_GPIO_Port, DrillingDir_Pin, RESET);
      }
   }

///////////////////////////////////////////////////////////////////////////////////
void vBoard_DrillingMotor_SetState(const SystemState_T eSystemState)
///////////////////////////////////////////////////////////////////////////////////
   {
   HAL_GPIO_WritePin(DrillingEnbl_GPIO_Port, DrillingEnbl_Pin, eSystemState);
   }

///////////////////////////////////////////////////////////////////////////////////
void vBoard_DrillingMotor_SpeedMeasurement(uint8_t *u8Speed)
///////////////////////////////////////////////////////////////////////////////////
   {
   // TODO : Implementation
   }

///////////////////////////////////////////////////////////////////////////////////
void vBoard_DrillingMotor_CurrentMeasurement(uint16_t *pu16Current_mA)
///////////////////////////////////////////////////////////////////////////////////
   {
   /*
    * ADC channels :
    *    ADC_CHANNEL_15 - DrillingMotorCurrent
    *    ADC_CHANNEL_14 - LinearTemp
    *    ADC_CHANNEL_7 - DrillingTemp
    */
   ADC_ChannelConfTypeDef sConfig;
   float fCurrent;
   uint16_t u16AdcMeasurement;   // 12-bit ADC measurement value (0 - 4095)   (2048 with current 0A)
   float fAdcVoltage;            // Voltage at ADC pin (0-3.3V) (1.65V with current 0A)

   sConfig.Channel = ADC_CHANNEL_15;
   sConfig.Rank = 1;
   sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
   if(HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
      {
      Error_Handler();
      }

   HAL_ADC_Start(&hadc1);
   if(HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK)
      {
      u16AdcMeasurement = HAL_ADC_GetValue(&hadc1);  //get ADC value
      fAdcVoltage = ((float) u16AdcMeasurement * 3.3) / 4095;  //calculate voltage on ADC pin
      fCurrent = 1000.0 * (fAdcVoltage - 1.65) / (AD8418_Gain * AD8418_Res);  //calculate current in mA on input pins of AD8418 chip

      *pu16Current_mA = (uint16_t) fCurrent;
      }
   }

///////////////////////////////////////////////////////////////////////////////////
void vBoard_DrillingMotor_TemperatureMeasurement(int16_t *pi16Temp)
///////////////////////////////////////////////////////////////////////////////////
   {
   /*
    * ADC channels :
    *    ADC_CHANNEL_15 - DrillingMotorCurrent
    *    ADC_CHANNEL_14 - LinearTemp
    *    ADC_CHANNEL_7 - DrillingTemp
    */
   ADC_ChannelConfTypeDef sConfig;

   uint16_t u16AdcMeasurement;   // 12-bit ADC measurement value (0 - 4095)   (2048 with current 0A)
   float fAdcVoltage;            // Voltage at ADC pin (0-3.3V) (1.65V with current 0A)
   float fDrillingTemp;

   sConfig.Channel = ADC_CHANNEL_7;
   sConfig.Rank = 1;
   sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
   if(HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
      {
      Error_Handler();
      }

   HAL_ADC_Start(&hadc1);
   if(HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK)
      {
      u16AdcMeasurement = HAL_ADC_GetValue(&hadc1);                        //get ADC value
      fAdcVoltage = ((float) u16AdcMeasurement * 3.3) / 4095;              //calculate voltage on ADC pin
      fDrillingTemp = 10 * (fAdcVoltage - 0.5) / MCP9700_TempCoefficient;  //precision 0.1*C => value 432 = 43.2*C

      *pi16Temp = (int16_t) fDrillingTemp;
      }
   }

/*****************************************************
 *            LINEAR MOTOR FUNCTIONS                 *
 ****************************************************/

///////////////////////////////////////////////////////////////////////////////////
void vBoard_LinearMotor_Update(void)
///////////////////////////////////////////////////////////////////////////////////
   {
   int16_t i16Temp;
   vBoard_LinearMotor_TemperatureMeasurement(&i16Temp);
   DreamExperiment_setLinearMotorTemperature(i16Temp);

   SystemState_T eSystemState;
   DreamExperiment_getLinearMotorState(&eSystemState);
   vBoard_LinearMotor_SetState(eSystemState);

   LinearMotorMode_T eLinearMotorMode;
   DreamExperiment_getLinearMotorMode(&eLinearMotorMode);
   vBoard_LinearMotor_SetMode(eLinearMotorMode);

   MotorDirection_T eMotorDirection;
   DreamExperiment_getLinearMotorDirection(&eMotorDirection);
   vBoard_LinearMotor_SetDirection(eMotorDirection);

   uint16_t u16LinearSpeed;
   DreamExperiment_getLinearMotorSpeed(&u16LinearSpeed);
   vBoard_LinearMotor_SetSpeed(u16LinearSpeed);

   Signal_T eSignal;
   DreamExperiment_getLOsignal(&eSignal);
   if(Signal_On == eSignal)
      {
      HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4);
      }
   else
      {
      HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_4);
      }
   }

///////////////////////////////////////////////////////////////////////////////////
void vBoard_LinearMotor_SetState(const SystemState_T eSystemState)
///////////////////////////////////////////////////////////////////////////////////
   {
   if(SystemState_Enable == eSystemState)    //Negative Logic
      {
      HAL_GPIO_WritePin(LinearEnbl_GPIO_Port, LinearEnbl_Pin, RESET);
      }
   else
      {
      HAL_GPIO_WritePin(LinearEnbl_GPIO_Port, LinearEnbl_Pin, SET);
      }
   }
///////////////////////////////////////////////////////////////////////////////////
void vBoard_LinearMotor_SetDirection(const MotorDirection_T eMotorDirection)
///////////////////////////////////////////////////////////////////////////////////
   {
   HAL_GPIO_WritePin(LinearDir_GPIO_Port, LinearDir_Pin, eMotorDirection);
// TODO : Implementation
   }

///////////////////////////////////////////////////////////////////////////////////
void vBoard_LinearMotor_SetMode(const LinearMotorMode_T eLinearMode)
///////////////////////////////////////////////////////////////////////////////////
   {
   uint8_t mode = (uint8_t) eLinearMode;

   HAL_GPIO_WritePin(Mode0_GPIO_Port, Mode0_Pin, (mode & 0x01) ? SET : RESET);
   HAL_GPIO_WritePin(Mode1_GPIO_Port, Mode1_Pin, (mode & 0x02) ? SET : RESET);
   HAL_GPIO_WritePin(Mode2_GPIO_Port, Mode2_Pin, (mode & 0x04) ? SET : RESET);
   }

///////////////////////////////////////////////////////////////////////////////////
void vBoard_LinearMotor_CheckFault(void)
///////////////////////////////////////////////////////////////////////////////////
   {
   // TODO : Implementation
   }

///////////////////////////////////////////////////////////////////////////////////
void vBoard_LinearMotor_TemperatureMeasurement(int16_t *pi16Temp)
///////////////////////////////////////////////////////////////////////////////////
   {
   /*
    * ADC channels :
    *    ADC_CHANNEL_15 - DrillingMotorCurrent
    *    ADC_CHANNEL_14 - LinearTemp
    *    ADC_CHANNEL_7  - DrillingTemp
    */
   ADC_ChannelConfTypeDef sConfig;

   float fLinearTemp;
   uint16_t u16AdcMeasurement;   // 12-bit ADC measurement value (0 - 4095)   (2048 with current 0A)
   float fAdcVoltage;            // Voltage at ADC pin (0-3.3V) (1.65V with current 0A)

   sConfig.Channel = ADC_CHANNEL_14;
   sConfig.Rank = 1;
   sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
   if(HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
      {
      Error_Handler();
      }

   HAL_ADC_Start(&hadc1);
   if(HAL_ADC_PollForConversion(&hadc1, 100) == HAL_OK)
      {
      u16AdcMeasurement = HAL_ADC_GetValue(&hadc1);                        //get ADC value
      fAdcVoltage = ((float) u16AdcMeasurement * 3.3) / 4095;              //calculate voltage on ADC pin
      fLinearTemp = 10 * (fAdcVoltage - 0.5) / MCP9700_TempCoefficient;    //precision 0.1*C => value 432 = 43.2*C

      *pi16Temp = (int16_t) fLinearTemp;
      }

   }

///////////////////////////////////////////////////////////////////////////////////
void vBoard_LinearMotor_SetSpeed(const uint16_t u16LinearSpeed)
///////////////////////////////////////////////////////////////////////////////////
   {

   if(0 >= u16LinearSpeed || 20000 <= u16LinearSpeed)
      {  // there can not be value out of range
         // value should be between 0 and 20k
      return;
      }

   TIM_MasterConfigTypeDef sMasterConfig;
   TIM_OC_InitTypeDef sConfigOC;

   htim2.Instance = TIM2;
   htim2.Init.Prescaler = 1199;
   htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
   htim2.Init.Period = (20000 / u16LinearSpeed) - 1;
   htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
   if(HAL_TIM_PWM_Init(&htim2) != HAL_OK)
      {
      Error_Handler();
      }

   sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
   sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
   if(HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
      {
      Error_Handler();
      }

   sConfigOC.OCMode = TIM_OCMODE_PWM1;
   sConfigOC.Pulse = htim2.Init.Period / 2;
   sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
   sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
   if(HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
      {
      Error_Handler();
      }

   HAL_TIM_MspPostInit(&htim2);
   }

