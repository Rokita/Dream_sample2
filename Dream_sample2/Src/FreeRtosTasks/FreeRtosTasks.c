/*
 * FreeRtosTasks.c
 *
 *  Created on: 3 pa� 2016
 *      Author: Kamil
 */

#include "FreeRtosTasks/FreeRtosTasks.h"

#include "FreeRTOS.h"
#include "task.h"
//#include "timers.h"
//#include "queue.h"
//#include "semphr.h"

//////////////////////////////////////////////////////
void vSignalsAndMeasurementsTask(void *parameters)
//////////////////////////////////////////////////////
   {
   while(1)
      {
      vBoard_Signals_Update();
      vBoard_LimitSwitch_Update();
      vBoard_DrillingMotor_Update();
      vBoard_LinearMotor_Update();

      vTaskDelay(50 / portTICK_PERIOD_MS);
      }
   /* FUNCTION SHOULDN'T NEVER BE HERE */
   }

//////////////////////////////////////////////////////
void vMainApplicationTask(void *parameters)
//////////////////////////////////////////////////////
   {
   while(1)
      {
      vApplicationMain();
      vTaskDelay(200 / portTICK_PERIOD_MS);
      }
   /* FUNCTION SHOULDN'T NEVER BE HERE */
   }

//////////////////////////////////////////////////////
void vCommunicationTask(void *parameters)
//////////////////////////////////////////////////////
   {
   while(1)
      {
      vTaskDelay(300 / portTICK_PERIOD_MS);
      HAL_GPIO_TogglePin(Led3_GPIO_Port, Led3_Pin);
      }
   /* FUNCTION SHOULDN'T NEVER BE HERE */
   }
