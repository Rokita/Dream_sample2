/*
 * board.h
 *
 *  Created on: 3 pa� 2016
 *      Author: Kamil
 */

#ifndef BOARD_BOARD_H_
#define BOARD_BOARD_H_

#include "mxconstants.h"
#include "stm32f1xx_hal.h"

#include "DreamExperiment/DreamExperiment.h"

TIM_HandleTypeDef htim2;
ADC_HandleTypeDef hadc1;
UART_HandleTypeDef huart1;
SPI_HandleTypeDef hspi2;


/*****************************************************
 *                BOARD FUNCTIONS                    *
 ****************************************************/

void vBoard_LED_Set(const uint8_t LedNumber, const uint8_t state);
void vBoard_LED_Toggle(const uint8_t LedNumber);

/*****************************************************
 *                SIGNALS FUNCTIONS                  *
 ****************************************************/

void vBoard_Signals_Update(void);

/*****************************************************
 *            LIMIT SWITCH FUNCTIONS                 *
 ****************************************************/

void vBoard_LimitSwitch_Update(void);

/*****************************************************
 *          DRILLING MOTOR FUNCTIONS                 *
 ****************************************************/

void vBoard_DrillingMotor_Update(void);

/*****************************************************
 *            LINEAR MOTOR FUNCTIONS                 *
 ****************************************************/

void vBoard_LinearMotor_Update(void);

//////////////////////////////////////////////////////
//                END OF FILE                       //
//////////////////////////////////////////////////////

#endif /* BOARD_BOARD_H_ */
