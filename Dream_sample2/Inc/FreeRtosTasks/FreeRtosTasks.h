/*
 * FreeRtosTasks.h
 *
 *  Created on: 3 pa� 2016
 *      Author: Kamil
 */

#ifndef FREERTOS_FREERTOSTASKS_H_
#define FREERTOS_FREERTOSTASKS_H_

#include <mxconstants.h>
#include "stm32f1xx_hal.h"

#include "Board/board.h"
#include "Application/Application.h"

void vSignalsAndMeasurementsTask(void *);
void vMainApplicationTask(void *);
void vCommunicationTask(void *);

#endif /* FREERTOS_FREERTOSTASKS_H_ */


