/**
 ******************************************************************************
 * File Name          : mxconstants.h
 * Description        : This file contains the common defines of the application
 ******************************************************************************
 *
 * COPYRIGHT(c) 2016 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MXCONSTANT_H
#define __MXCONSTANT_H
/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define SignalSods_Pin GPIO_PIN_13
#define SignalSods_GPIO_Port GPIOC
#define SignalSoe_Pin GPIO_PIN_14
#define SignalSoe_GPIO_Port GPIOC
#define SignalLo_Pin GPIO_PIN_15
#define SignalLo_GPIO_Port GPIOC
#define LynxPin5_Pin GPIO_PIN_0
#define LynxPin5_GPIO_Port GPIOC
#define LynxPin4_Pin GPIO_PIN_1
#define LynxPin4_GPIO_Port GPIOC
#define LynxPin3_Pin GPIO_PIN_2
#define LynxPin3_GPIO_Port GPIOC
#define LynxPin2_Pin GPIO_PIN_3
#define LynxPin2_GPIO_Port GPIOC
#define Mode2_Pin GPIO_PIN_0
#define Mode2_GPIO_Port GPIOA
#define Mode1_Pin GPIO_PIN_1
#define Mode1_GPIO_Port GPIOA
#define Mode0_Pin GPIO_PIN_2
#define Mode0_GPIO_Port GPIOA
#define LinearStep_Pin GPIO_PIN_3
#define LinearStep_GPIO_Port GPIOA
#define LinearEnbl_Pin GPIO_PIN_4
#define LinearEnbl_GPIO_Port GPIOA
#define LinearDir_Pin GPIO_PIN_5
#define LinearDir_GPIO_Port GPIOA
#define LinearFault_Pin GPIO_PIN_6
#define LinearFault_GPIO_Port GPIOA
#define Temp1_Pin GPIO_PIN_7
#define Temp1_GPIO_Port GPIOA
#define Temp2_Pin GPIO_PIN_4
#define Temp2_GPIO_Port GPIOC
#define DrillingCurrent_Pin GPIO_PIN_5
#define DrillingCurrent_GPIO_Port GPIOC
#define DrillingFb_Pin GPIO_PIN_0
#define DrillingFb_GPIO_Port GPIOB
#define DrillingEnbl_Pin GPIO_PIN_1
#define DrillingEnbl_GPIO_Port GPIOB
#define DrillingDir_Pin GPIO_PIN_2
#define DrillingDir_GPIO_Port GPIOB
#define LimitSwitchZero_Pin GPIO_PIN_10
#define LimitSwitchZero_GPIO_Port GPIOB
#define LimitSwitchMax_Pin GPIO_PIN_11
#define LimitSwitchMax_GPIO_Port GPIOB
#define MemoryHold_Pin GPIO_PIN_12
#define MemoryHold_GPIO_Port GPIOB
#define MemorySck_Pin GPIO_PIN_13
#define MemorySck_GPIO_Port GPIOB
#define MemoryMiso_Pin GPIO_PIN_14
#define MemoryMiso_GPIO_Port GPIOB
#define MemoryMosi_Pin GPIO_PIN_15
#define MemoryMosi_GPIO_Port GPIOB
#define MemoryCs_Pin GPIO_PIN_6
#define MemoryCs_GPIO_Port GPIOC
#define MemoryWp_Pin GPIO_PIN_7
#define MemoryWp_GPIO_Port GPIOC
#define Led1_Pin GPIO_PIN_5
#define Led1_GPIO_Port GPIOB
#define CommunicationTx_Pin GPIO_PIN_6
#define CommunicationTx_GPIO_Port GPIOB
#define CommunicationRx_Pin GPIO_PIN_7
#define CommunicationRx_GPIO_Port GPIOB
#define Led2_Pin GPIO_PIN_8
#define Led2_GPIO_Port GPIOB
#define Led3_Pin GPIO_PIN_9
#define Led3_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
 * @}
 */

/**
 * @}
 */

#endif /* __MXCONSTANT_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
