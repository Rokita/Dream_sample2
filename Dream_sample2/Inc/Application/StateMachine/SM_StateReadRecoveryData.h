/*
 * SM_StateReadRecoveryData.h
 *
 *  Created on: 12 pa� 2016
 *      Author: Kamil
 */

#ifndef APPLICATION_STATEMACHINE_SM_STATEREADRECOVERYDATA_H_
#define APPLICATION_STATEMACHINE_SM_STATEREADRECOVERYDATA_H_

#include "Board/board.h"
#include "Application/StateMachine/StateMachine.h"

void SM_StateReadRecoveryData(void);

#endif /* APPLICATION_STATEMACHINE_SM_STATEREADRECOVERYDATA_H_ */
