/*
 * SM_StateTestMode.h
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#ifndef APPLICATION_STATEMACHINE_SM_STATETESTMODE_H_
#define APPLICATION_STATEMACHINE_SM_STATETESTMODE_H_

#include "Board/board.h"
#include "Application/StateMachine/StateMachine.h"


void SM_StateTestMode(void);

#endif /* APPLICATION_STATEMACHINE_SM_STATETESTMODE_H_ */
