/*
 * SM_StateInit.h
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#ifndef APPLICATION_STATEMACHINE_SM_STATEINIT_H_
#define APPLICATION_STATEMACHINE_SM_STATEINIT_H_

#include "Board/board.h"
#include "Application/StateMachine/StateMachine.h"

void SM_StateInit(void);


#endif /* APPLICATION_STATEMACHINE_SM_STATEINIT_H_ */
