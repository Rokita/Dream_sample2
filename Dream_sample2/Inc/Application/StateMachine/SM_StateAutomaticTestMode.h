/*
 * SM_StateAutomaticTestMode.h
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#ifndef APPLICATION_STATEMACHINE_SM_STATEAUTOMATICTESTMODE_H_
#define APPLICATION_STATEMACHINE_SM_STATEAUTOMATICTESTMODE_H_

#include "Board/board.h"
#include "Application/StateMachine/StateMachine.h"

void SM_StateAutomaticTestMode(void);


#endif /* APPLICATION_STATEMACHINE_SM_STATEAUTOMATICTESTMODE_H_ */
