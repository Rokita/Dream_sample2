/*
 * StateMachine.h
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#ifndef APPLICATION_STATEMACHINE_STATEMACHINE_H_
#define APPLICATION_STATEMACHINE_STATEMACHINE_H_

#include "SM_StateInit.h"
#include "SM_StateTestMode.h"
#include "SM_StateAutomaticTestMode.h"
#include "SM_StateFlightMode.h"
#include "SM_StateReadRecoveryData.h"

typedef enum State
   {
   StateMachine_StateInit,
   StateMachine_StateTestMode,
   StateMachine_StateAutomaticTestMode,
   StateMachine_StateFlightMode,
   StateMachine_StateReadRecoveryData,
   StateMachine_LastUnused
   } State_T;

typedef void (*Handler[StateMachine_LastUnused])(void);

extern Handler SM_Function;

typedef struct StateMachine
   {
      State_T LastState;
      State_T CurrentState;
   } StateMachine_T;

void SM_transitionTo(State_T nextState);
State_T SM_GetCurrentState(void);
State_T SM_GetLastState(void);

#endif /* APPLICATION_STATEMACHINE_STATEMACHINE_H_ */
