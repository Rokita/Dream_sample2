/*
 * Application.h
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#ifndef APPLICATION_APPLICATION_H_
#define APPLICATION_APPLICATION_H_

#include "DreamExperiment/DreamExperiment.h"
#include "Application/StateMachine/StateMachine.h"
#include "Board/board.h"


void vApplicationInit(void);
void vApplicationMain(void);


#endif /* APPLICATION_APPLICATION_H_ */
