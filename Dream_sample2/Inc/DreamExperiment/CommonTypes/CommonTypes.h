/*
 * CommonTypes.h
 *
 *  Created on: 6 pa� 2016
 *      Author: Kamil
 */

#ifndef DREAMEXPERIMENT_COMMONTYPES_COMMONTYPES_H_
#define DREAMEXPERIMENT_COMMONTYPES_COMMONTYPES_H_

#include <stdint.h>

typedef enum MotorDirection
   {
   MotorDirection_Left,
   MotorDirection_Right,
   MotorDirection_LastUnused
   } MotorDirection_T;

typedef enum SystemState
   {
   SystemState_Disable,
   SystemState_Enable,
   SystemState_LastUnused
   } SystemState_T;

typedef enum FlightModeSM
   {
   FlightModeSM_StateInit,
   FlightModeSM_StateCountdown,
   FlightModeSM_StateSODS,
   FlightModeSM_StateLO,
   FlightModeSM_StateSOE,
   FlightModeSM_StateBTZP,
   FlightModeSM_StateEOF,
   FlightModeSM_LastUnused
   } FlightModeSM_T;

#endif /* DREAMEXPERIMENT_COMMONTYPES_COMMONTYPES_H_ */
