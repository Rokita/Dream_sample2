/*
 * LinearMotor.h
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#ifndef DREAMEXPERIMENT_LINEARMOTOR_LINEARMOTOR_H_
#define DREAMEXPERIMENT_LINEARMOTOR_LINEARMOTOR_H_

#include "../CommonTypes/CommonTypes.h"

typedef enum LinearMotorMode
   {
   LinearMotorMode_FullStep,
   LinearMotorMode_2Microsteps,
   LinearMotorMode_4Microsteps,
   LinearMotorMode_8Microsteps,
   LinearMotorMode_16Microsteps,
   LinearMotorMode_32Microsteps,
   LinearMotorMode_32Microsteps2,
   LinearMotorMode_32Microsteps3,
   LinearMotorMode_LastUnused
   } LinearMotorMode_T;

typedef enum LinearMotorFault
   {
   LinearMotorFault_NO,
   LinearMotorFault_YES,
   LinearMotorFault_LastUnused
   }LinearMotorFault_t;

typedef struct LinearMotor
   {
      SystemState_T eSystemState;
      LinearMotorMode_T eLinearMotorMode;
      MotorDirection_T eMotorDirection;
      LinearMotorFault_t eFault;
      int16_t i16Temperature; //temperature = u16Temperature/10
      uint16_t u16Speed;
   } LinearMotor_T;
#endif /* DREAMEXPERIMENT_LINEARMOTOR_LINEARMOTOR_H_ */
