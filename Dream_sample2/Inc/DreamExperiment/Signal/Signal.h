/*
 * Signal.h
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#ifndef DREAMEXPERIMENT_SIGNAL_SIGNAL_H_
#define DREAMEXPERIMENT_SIGNAL_SIGNAL_H_

typedef enum Signal
   {
   Signal_Off,
   Signal_On,
   Signal_LastUnused
   }Signal_T;


#endif /* DREAMEXPERIMENT_SIGNAL_SIGNAL_H_ */
