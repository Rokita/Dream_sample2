/*
 * DrillingMotor.h
 *
 *  Created on: 6 pa� 2016
 *      Author: Kamil
 */

#ifndef DREAMEXPERIMENT_DRILLINGMOTOR_DRILLINGMOTOR_H_
#define DREAMEXPERIMENT_DRILLINGMOTOR_DRILLINGMOTOR_H_

#include "../CommonTypes/CommonTypes.h"

typedef struct DrillingMotor
   {
      SystemState_T eSystemState;
      MotorDirection_T eMotorDirection;
      uint8_t u8MeasuredSpeed;
      uint16_t u16Current_mA;  // Drilling Motor Current (0 - 900 mA)
      int16_t i16Temperature; //temperature = u16Temperature/10
   }DrillingMotor_T;

#endif /* DREAMEXPERIMENT_DRILLINGMOTOR_DRILLINGMOTOR_H_ */
