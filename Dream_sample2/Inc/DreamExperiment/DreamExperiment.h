/*
 * DreamExperiment.h
 *
 *  Created on: 4 pa� 2016
 *      Author: Kamil
 */

#ifndef DREAMEXPERIMENT_DREAMEXPERIMENT_H_
#define DREAMEXPERIMENT_DREAMEXPERIMENT_H_

#include "Signal/Signal.h"
#include "DreamExperiment/LimitSwitch/LimitSwitch.h"
#include "DreamExperiment/LinearMotor/LinearMotor.h"
#include "DreamExperiment/DrillingMotor/DrillingMotor.h"
#include "DreamExperiment/CommonTypes/CommonTypes.h"


typedef struct DreamExperiment
   {
      FlightModeSM_T eFlightModeState;

      Signal_T eLiftOff;
      Signal_T eStartOfExperiment;
      Signal_T eStartOfDataStorage;

      LimitSwitch_T eLimitSwitch_Zero;
      LimitSwitch_T eLimitSwitch_Max;

      DrillingMotor_T kDrillingMotor;

      LinearMotor_T kLinearMotor;

   } DreamExperiment_T;

/**************************************************************
 *                  Initialization function                   *
 *************************************************************/
void DreamExperiment_StructInitialization(void);

/**************************************************************
 *        Flight Mode State Machine functions                 *
 *************************************************************/
void DreamExperiment_setFlightModeState(const FlightModeSM_T);

void DreamExperiment_getFlightModeState(FlightModeSM_T *);

/**************************************************************
 *                    Signals functions                       *
 *************************************************************/
void DreamExperiment_setLOsignal(const Signal_T);
void DreamExperiment_setSOEsignal(const Signal_T);
void DreamExperiment_setSODSsignal(const Signal_T);

void DreamExperiment_getLOsignal(Signal_T *);
void DreamExperiment_getSOEsignal(Signal_T *);
void DreamExperiment_getSODSsignal(Signal_T *);

/**************************************************************
 *                  Limit Switch functions                    *
 *************************************************************/
void DreamExperiment_setLimitSwitchZero(const LimitSwitch_T);
void DreamExperiment_setLimitSwitchMax(const LimitSwitch_T);

void DreamExperiment_getLimitSwitchZero(LimitSwitch_T *);
void DreamExperiment_getLimitSwitchMax(LimitSwitch_T *);

/**************************************************************
 *                Drilling Motor functions                    *
 *************************************************************/
void DreamExperiment_setDrillingMotorState(const SystemState_T);
void DreamExperiment_setDrillingMotorDirection(const MotorDirection_T);
void DreamExperiment_setDrillingMotorMeasuredSpeed(const uint8_t);
void DreamExperiment_setDrillingMotorCurrent(const uint16_t);
void DreamExperiment_setDrillingMotorTemperature(const int16_t);

void DreamExperiment_getDrillingMotorState(SystemState_T *);
void DreamExperiment_getDrillingMotorDirection(MotorDirection_T *);
void DreamExperiment_getDrillingMotorMeasuredSpeed(uint8_t *);
void DreamExperiment_getDrillingMotorCurrent(uint16_t *);
void DreamExperiment_getDrillingMotorTemperature(int16_t *);

/**************************************************************
 *                  Linear Motor functions                    *
 *************************************************************/
void DreamExperiment_setLinearMotorState(const SystemState_T);
void DreamExperiment_setLinearMotorMode(const LinearMotorMode_T);
void DreamExperiment_setLinearMotorDirection(const MotorDirection_T);
void DreamExperiment_setLinearMotorFault(const LinearMotorFault_t);
void DreamExperiment_setLinearMotorTemperature(const int16_t);
void DreamExperiment_setLinearMotorSpeed(const uint16_t);

void DreamExperiment_getLinearMotorState(SystemState_T *);
void DreamExperiment_getLinearMotorMode(LinearMotorMode_T *);
void DreamExperiment_getLinearMotorDirection(MotorDirection_T *);
void DreamExperiment_getLinearMotorFault(LinearMotorFault_t *);
void DreamExperiment_getLinearMotorTemperature(int16_t *);
void DreamExperiment_getLinearMotorSpeed(uint16_t *);

//////////////////////////////////////////////////////////////
//                         END OF FILE                      //
//////////////////////////////////////////////////////////////
#endif /* DREAMEXPERIMENT_DREAMEXPERIMENT_H_ */
