/*
 * LimitSwitch.h
 *
 *  Created on: 6 pa� 2016
 *      Author: Kamil
 */

#ifndef DREAMEXPERIMENT_LIMITSWITCH_LIMITSWITCH_H_
#define DREAMEXPERIMENT_LIMITSWITCH_LIMITSWITCH_H_

typedef enum LimitSwitch
   {
   LimitSwitch_Off,
   LimitSwitch_On,
   LimitSwitch_LastUnused
   }LimitSwitch_T;

#endif /* DREAMEXPERIMENT_LIMITSWITCH_LIMITSWITCH_H_ */
